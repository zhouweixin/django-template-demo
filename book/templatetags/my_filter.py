# Author: zhouweixin
# @Time: 2019-07-09 15:23
# @File: my_filter.py
# @Description: 过滤器最多只能有两个参数, 且第1个参数永远是被过滤的值. 而且所在的app要被install, 且包名为templatetags

from django import template
from datetime import datetime


register = template.Library()


@register.filter
def greet(value, word):
    """

    :param value:
    :param word:
    :return:
    """
    return value + word


@register.filter
def time_since(value):
    """
    time距离现在的时间间隔
    1. 如果时间间隔小于1分钟以内, 那么就显示"刚刚"
    2. 如果是大于1分钟小于1小时, 那么就显示"xx分钟前"
    3. 如果是大于1小时小于24小时, 那么就显示"xx小时前"
    4. 如果是大于24小时小于30天以内, 那么就显示"xx天前"
    5. 否则就显示具体的时间
    :param value:
    :return:
    """
    if not isinstance(value, datetime):
        value.strftime("%Y-%m%d %H%i")

    now = datetime.now()
    # timedelay.total_seconds
    seconds = (now - value).total_seconds()
    minutes = 0
    hours = 0

    if seconds < 60:
        return "刚刚"
    elif seconds < 60*60:
        minutes = int(seconds / 60)
        return "%s分钟前" % minutes
    elif minutes < 60*24:
        hours = int(minutes / 60)
        return "%s小时前" % hours
    elif hours < 24 * 30:
        days = int(hours / 24)
        return "%s天前" % hours
    else:
        return value.strftime("%Y-%m%d %H%i")



# 注册: 2种方式, 一种是用注解的装饰模式, 一种是下面的代码
# register.filter("greet", greet)