# Author: zhouweixin
# @Time: 2019-07-09 11:43
# @File: urls.py
# @Description:

from django.urls import path
from . import views

urlpatterns = [
    path('<id>', views.find_by_id),
]

