from django.shortcuts import render
from datetime import datetime


def index(request):
    return render(request, "index.html")


class Person:
    def __init__(self, name):
        self.name = name


def variable(request):
    context = {
        "name": "张三",
        "names": ["周一", "周二", "周三"],
        "person": Person("李四"),
    }
    return render(request, "variable.html", context=context)


def tag(request):
    context = {
        "age": 14,
        "fruits": ["苹果1", "香蕉2"],
        "person": Person("李四"),
        "link": "<a href='https://bytedance.com'>进入字节跳动</a>"
    }
    return render(request, "tag.html", context=context)


def filter(request):
    context = {
        "i1": 1,
        "i2": 2,
        "s1": 'xy',
        "s2": 'z',
        "l1": ['a', 'b'],
        "l2": ['c', 'd'],

        "birthday": datetime.now(),
        "v1": False,
        "v2": [],
        "v3": "",
        "v4": None,
        "v5": {},
        "v6": 0,

        "js": "<script>console.log('Hello world');</script>",
        "arr": list(range(10)),

        "sentence": "我们都是好孩子。",
        "sentence_html": "<p>我们都是好孩子。</p>",

        "my_time": datetime(year=2019, month=7, day=9, hour=16, minute=0, second=0),
    }
    return render(request, "filter.html", context=context)